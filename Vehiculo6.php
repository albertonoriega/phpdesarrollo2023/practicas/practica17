<?php

class Vehiculo
{
    public $matricula;
    private $color;
    protected $encendido;
    public static $ruedas = 5;


    public function __construct($matricula, $color, $encendido)
    {
        $this->matricula = $matricula;
        $this->color = $color;
        $this->encendido = $encendido;
    }

    public static function encender()
    {
        $encendido = true;
        echo 'Vehiculo encendido <br/>';
    }

    public function apagar()
    {
        $this->encendido = false;
        echo 'Vehiculo apagado <br />';
    }

    static function mensaje()
    {
        echo "Este es mi coche";
    }

    static function ruedas()
    {
        echo Vehiculo::$ruedas;
    }
}
