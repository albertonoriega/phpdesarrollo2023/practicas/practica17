<?php
include_once 'Vehiculo7.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    $ford = new Vehiculo();
    var_dump($ford);


    $opel = new Vehiculo('5965PPP');
    var_dump($opel);

    $seat = new Vehiculo('5254UUU', 'rojo');
    var_dump($seat);

    $dacia = new Vehiculo('5784UUU', 'azul', true);
    var_dump($dacia);
    ?>
</body>

</html>