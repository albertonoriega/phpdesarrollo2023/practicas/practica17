<?php

class Coche
{
    public $color;
    public $numeroPuertas;
    public $marca;
    public $gasolina = 0;

    public function llenarTanque($gasolinaNueva)
    {
        $this->gasolina = $this->gasolina + $gasolinaNueva;
    }

    public function acelerar()
    {
        if ($this->gasolina > 0) {
            $this->gasolina = $this->gasolina - 1;
            return 'Gasolina restante: ' . $this->gasolina;
        }
    }
}
