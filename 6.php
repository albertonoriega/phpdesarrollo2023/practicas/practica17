<?php
include_once 'Vehiculo6.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    $ford = new Vehiculo('DHH2323', 'rojo', false);
    $ford->mensaje();
    var_dump($ford);
    echo $ford->ruedas();
    $ford::mensaje();
    Vehiculo::$ruedas = 6;

    // Para acceder a ruedas como es static hayq ue hacerlo así
    echo Vehiculo::$ruedas;
    // De esta forma da error
    //echo $ford->ruedas;

    Vehiculo::encender();
    ?>
</body>

</html>