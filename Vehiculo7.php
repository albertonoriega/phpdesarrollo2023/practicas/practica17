<?php

class Vehiculo
{
    public $matricula;
    private string $color = 'blanco';
    protected bool $encendido = false;

    public function __construct(...$datos)
    {
        $numero = count($datos);
        $nombre = "__construct{$numero}";
        if (method_exists($this, $nombre)) {
            call_user_func_array([$this, $nombre], $datos);
            // $this->nombre(...$datos);
        }
    }


    private function __construct3($matricula, $color, $encendido)
    {
        $this->matricula = $matricula;
        $this->color = $color;
        $this->encendido = $encendido;
    }
    public function __construct2($matricula, $color,)
    {
        $this->matricula = $matricula;
        $this->color = $color;
    }

    public function __construct1($matricula)
    {
        $this->matricula = $matricula;
    }

    public function encender()
    {
        $this->encendido = true;
        echo 'Vehiculo encendido <br/>';
    }

    public function apagar()
    {
        $this->encendido = false;
        echo 'Vehiculo apagado <br />';
    }
}
