<?php
class Usuario
{
    public $nombre = "defecto";

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    public function getNombre()
    {
        return $this->nombre;
    }
}

$persona = new  Usuario();
echo $persona->nombre;
$persona->nombre = "Silvia";
var_dump($persona);

// cambiar el nombre usando el setter
$persona->setNombre('Juan');
echo $persona->nombre;
// obtener el nombre con el getter
echo $persona->getNombre();
