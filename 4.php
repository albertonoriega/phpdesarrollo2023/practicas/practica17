<?php
include 'Coche4.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    $coche = new Coche();
    $coche->color = 'Rojo';
    $coche->marca = 'Honda';
    $coche->numeroPuertas = 4;
    $coche->llenarTanque(10);
    echo $coche->acelerar();
    echo $coche->acelerar();
    echo $coche->acelerar();
    var_dump($coche);
    ?>
</body>

</html>